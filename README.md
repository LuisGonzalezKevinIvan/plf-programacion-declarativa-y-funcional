# 1. Programación Declarativa. Orientaciones y pautas para el estudio (1998, 1999, 2001)
```plantuml
@startmindmap
<style>
mindmapDiagram {
  .nodoPrin {
    BackgroundColor #27AA80
  }
  .nodoSec {
    BackgroundColor #32FF6A
  }
  .nodoTer {
    BackgroundColor #A8FF3E
  }
  .nodoCuar {
    BackgroundColor #F4FF61 
  }
  .nodoQuin {
    BackgroundColor #96F7D2
  }
  .nodoSex {
    BackgroundColor #F0F696
  }
}
</style>
* Programación<<nodoPrin>>
	*_ Es<<nodoSec>>
		* Indiacarle a la maquina lo que se requiere que haga.<<nodoTer>>
	* Evolución<<nodoSec>>
		* En un principio se hablaba en lenguaje maquina.<<nodoTer>>
		*_ Consistia en secuencia de ordenes
			* Directas.<<nodoCuar>>
			* Concretas.<<nodoCuar>>
			* Sencillas.<<nodoCuar>>
		*_ Al inicio
			* Los programas<<nodoTer>>
				*_ Consistían
					* Secuencias de ordenes directas.<<nodoCuar>>
				*_ Lenguajes
					* Bajo nivel.<<nodoCuar>>
					* Alto nivel.<<nodoCuar>>
	* Partes<<nodoSec>>
		* Burocrática<<nodoTer>>
			* La gestión es detallada en la memoria.<<nodoCuar>>
			* Secuencia de las ordenes.<<nodoCuar>>
		* Creativa<<nodoTer>>  
			* Creación del algoritmo.<<nodoCuar>>
	* Tipos<<nodoSec>>
		*_ Surgen por
			* Problemas en la programación clasica.<<nodoTer>>
		* Declarativa<<nodoTer>>
			*_ Consiste
				* Estilo de programación.<<nodoTer>>
				* Hacer enfasis en los aspectos creativos \npara lograr resolver el problema.<<nodoTer>>
				* Abandonar las secuencias de órdenes que \ngestionan la memoria del ordenador.<<nodoTer>>
				* Tareas rutinarias de progrmación se dejan al compilador <<nodoTer>>
					*_ Utilidad
						* Mejor tiempo de modificación y verificación.<<nodoCuar>>
						* Mas preciosión en la practica.<<nodoCuar>>
			* Variantes<<nodoCuar>>
				* Funcional<<nodoQuin>>
					*_ Es
						* La aplicación de funciones a unos datos.<<nodoSex>>
							*_ Donde
								* La aplicación de funciones se hacen mediante reglas de reescritura
									*_ Para
										* Alcanzar el nivel de descripción superior.
					* Toma el modelo donde los matemáticos definen funciones<<nodoSex>>
						*_ A través de
							* Simplifacar expresiones.
							* Reglas de reescritura.
					*_ Caracteristicas
						* Define tipos de datos infinitos.<<nodoSex>>
							*_ Ejemplo 
								* Serie de números Fibonacci
						* Funciones de orden superior.<<nodoSex>>
							* Toman una o más funciones como entrada.
							* Devuelven una función como salida.
							*_ Se aplica a
								* Programas que modifican programas.
						* Evaluación perezosa<<nodoSex>>
							*_ Trata de
								* Calcular lo que es necesario para ser cálculos posteriores.
								* Evalúar las ecuaciones.
					*_ Ventajas
						* Capacidad muy potente de los lenguajes funcionales.<<nodoSex>>
						* Funciones de orden superior<<nodoSex>>
							* Objetos principales
								*_ Los cuales son
									* Funciones que pueden definir funciones que actúan sobre funciones.
					*_ Mediante 
						* Lenguajes que utilizan los matemáticos (detallar funciones).<<nodoSex>>
				* Lógica<<nodoQuin>>
					*_ Son 
						* Predicados de primer orden.<<nodoSex>>
					*_ Se basa que
						* El compilador es un motor de interferencia.<<nodoSex>>
							*_ A partir de expresiones
								* Razona 
								* Da respuesta
						* Modelo de la demostración lógica y automatica<<nodoSex>>
							*_ Su uso
								* Axiomas
								* Reglas de inferencia.
								* Expresiones de los programas.
					*_ Características
						* No establece orden entre argumentos de entrada y datos de salida.<<nodoSex>>
						* Define funciones que actúan sobre funciones, \nno solo sobre los datos primitivos.<<nodoSex>>
					*_ Ventajas
						* Permite se mas declarativo.<<nodoSex>>
						* Establece dirección de procesamiento \nmediante argumentos y resultados.<<nodoSex>>
			*_ Programas más
				* Cortos.<<nodoSex>>
				* Faciles de realizar.<<nodoSex>>
				* Faciles de depurar.<<nodoSex>>
			*_ Propósitos
				* Evitar riesgos de cometer errores al hacer programas.<<nodoSex>>
				* Liberarse de asignaciones y detallar el \ncontrol de la gestión de memoria en el ordenador.<<nodoSex>>
				* Reducción en la complejidad de los programas.<<nodoSex>>
			*_ Ventajas
				* Tiempo de desarrollo más corto.<<nodoSex>>
				* Menor tiempo de depuración y modificación.<<nodoSex>>
				* Facilita la programación.<<nodoSex>>
				* El tiempo de desarrollo de una aplicación es una décima parte.<<nodoSex>>
			*_ Inconvenientes
				* Ideas abstractas.<<nodoSex>>
				* Al descubrir programas de alto nivel el compilador es el que toma las decisiones de manera automatica.<<nodoSex>>
					*_ Significaba 
						* Que es dificil que sean eficientes.
				*_ Ordenación 
					* Es importante cada asignación en la memoria.<<nodoSex>>
					* Crucial para resolver de forma eficiente.<<nodoSex>>
		* Imperativa<<nodoTer>>
			*_ Es 
				* Dar instrucciónes, comandos uno \ndetras de otro al ordenador.<<nodoCuar>>
			*_ Tipos 
				* Estructurada.<<nodoCuar>>
				* Procedimental.<<nodoCuar>>
				* Modular.<<nodoCuar>>
			*_ Segun el tipo de
				* Lenguajes<<nodoCuar>>
				* Recursos<<nodoCuar>>
		* Operativa<<nodoTer>>
			* Se basa en el modelo de Von Neumann.<<nodoCuar>>
		* Orientada a inteligencia artifical<<nodoTer>>
			* Estudia como un modelo de programación imperativa.<<nodoCuar>>
			* Lenguaje<<nodoCuar>>
				* Hibrido<<nodoQuin>>
					* LISP<<nodoSex>>
						* Permite hacer un tipo de programación imperativa.
				* Estándar<<nodoQuin>>
					* PROLOG<<nodoSex>>
						* Representacion del paradigma declarativo.
					* HASKELL<<nodoSex>>
						* Se utiliza en sistemas difefenciales.
						* Evaluación perezosa.
@endmindmap
```

# 2. Lenguaje de Programación Funcional (2015)

```plantuml
@startmindmap
<style>
mindmapDiagram {
  .nodoPrin {
    BackgroundColor #28FFBF
  }
  .nodoSec {
    BackgroundColor #C2F784
  }
  .nodoTer {
    BackgroundColor #FFF47D
  }
  .nodoCuar {
    BackgroundColor #FFBF86
  }
  .nodoQuin {
    BackgroundColor #F6A9A9
  }
  .nodoSex {
    BackgroundColor #E4D8DC
  }
}
</style>
* Programación funcional<<nodoPrin>>
	* Caracteristicas<<nodoSec>>
		* Estado del computo.<<nodoTer>>
		* Las funciones pueden ser parámetros de otras funciones.<<nodoTer>>
		* Funciones en su concepción puramente matemática.<<nodoTer>>
		* Transferencia referencial.<<nodoTer>>
		* Funciones constantes.<<nodoTer>>
			* Devuelven siempre el mismo resultado.<<nodoCuar>>
		* El resultado de las funciones no dependen de las otras.<<nodoTer>>
		* Funciones de orden superior.<<nodoTer>>
			*_ Efectua
				* Tomar una o más funciones como entrada.<<nodoCuar>>
				* Devolver una función como salida.<<nodoCuar>>
		* Transferencia referencial.<<nodoTer>>
		* Funciones matemáticas<<nodoTer>>
			*_ Como resultado
				* Concepto de asignación.<<nodoCuar>>
				* Recursión.<<nodoCuar>>
				* Desaparece el concepto.<<nodoCuar>>
					* Variable<<nodoQuin>>
						*_ Se refiere a
							* Los parámetros de entrada de las funciones.<<nodoSex>>
					* Bucle<<nodoQuin>>
						* Funciones recursivas<<nodoSex>>
							* Aquellas en donde se hacen referencia a si mismas.
	* Currificación<<nodoSec>> 
		*_ En homenaje a
			* Haskell Brooks Curry.<<nodoTer>>
		*_ Es
			* Una función la cual recibe varias funciones \nde entrada y devuelve diferentes salidas.<<nodoTer>>
		*_ Sirve para
			* Utilizar funciones como argumentos de otras funciones.<<nodoTer>>
	* Memorización<<nodoSec>> 
		*_ Implementada en 
			* 1968<<nodoTer>>
		*_ Aporte de
			* Donald Michie<<nodoTer>>
		*_ Es 
			* Almacenar el valor de una expresión \ncuya evaluación fue realizada.<<nodoTer>>
	* Evaluación<<nodoSec>>
		* Estricta<<nodoTer>>
			* Hasta no evaluar lo más interno, no evalúas lo más externo.<<nodoCuar>>
		* No estricta<<nodoTer>>
			* Mas dificultad al implementar.<<nodoCuar>>
			* Se evalua de afuera hacia adentro.<<nodoCuar>>
		* En corto circuito<<nodoTer>>
			* Trabaja con operadores booleanos.<<nodoCuar>>
			* El primer argumento de la función OR evalúa y el \nresultado es verdadero, el valor total debe ser verdadero.<<nodoCuar>>
			* El segundo argumento no se efectua o evalúa si el primer \nargumento de la función AND evalúa y el resultado es falso.<<nodoCuar>>
	* Cálculo Lambda<<nodoSec>>
		*_ Introducido por
			* Alonzo Church<<nodoTer>>
			* Stephen Kleene<<nodoTer>>
		*_ Desarrollado en
			* 1930<<nodoTer>>
		*_ Años despues
			* Lo continuo Haskell Brooks<<nodoTer>>
				* Sento las bases de la programación funcional.<<nodoCuar>>
		*_ Surge
			* ALGOL<<nodoTer>>
				*_ En 
					* 1965<<nodoCuar>>
				*_ Creador 
					* Peter J. Ladin<<nodoCuar>>
		* Parecido al modelo de Von Neumann<<nodoTer>>
		*_ Es
			* Un sistema computacional bastante potente.<<nodoTer>>
				*_ Objetivo
					* Definición de una función.<<nodoCuar>>
					* Recursión.<<nodoCuar>>
					* Noción de aplicación de funciones.<<nodoCuar>>
	*_ Bucles son
		* Serie de instrucciones que se repiten \nun numero indeterminado y estan \ncontrolados por el estado del controlador.<<nodoTer>>
	*_ Las constantes
		* Equiparan a las funciones.<<nodoTer>>
	*_ Como consecuencia 
		* La asignación.<<nodoTer>>
	* Ventajas<<nodoTer>>
		* No hay dependencia entre funciones.<<nodoCuar>>
		* Brinda un entorno multiprocesador<<nodoCuar>>
			*_ Lo que significa
				* Terner funciones en paralelo.<<nodoQuin>>
		* Permite escribir codigo de manera rápida.<<nodoCuar>>
	* Desventaja<<nodoTer>>
		* Nivel de abstracción bastante alto.<<nodoCuar>>
	* Funciones<<nodoTer>>
		*_ Son
			* En su concepción puramente matematicas.<<nodoCuar>>
		*_ Pueden ser
			* Parámetros de otras funciones.<<nodoCuar>>
		* Tipos<<nodoCuar>>
			* Funciones de orden superior<<nodoQuin>>
				*_ Cumplen con 
					* Tomar una o varias funciones como entrada<<nodoSex>>
					* Devolver una función como salida.<<nodoSex>>
			* Función constante<<nodoQuin>>
				*_ Devuelve
					* Simpre el mismo resultado.<<nodoSex>>
@endtmindmap
```

